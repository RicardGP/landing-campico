var widthSide = $(".sideContact").width();
var width = $(".landingBody").width();
var heightB = $(".landingBody").height();
var offBody = $(".landingBody").offset();
var header = offBody.top - (parseInt($(".box").css("padding-top")))/2;
var footer = heightB - parseInt($(".box").css("padding-top"));

if(screen.width >= 1280){

	$( window ).scroll(function() {
		var heightS = $(".sideContact").height();
		var marginTSide = ($(window).height() - $(".sideContact section").height())/2 - parseInt($(".box").css("padding-top"));
		
		if ( $(this).scrollTop() <= header ) {
	    	$(".sideContact").css({
	    		"margin-top": marginTSide,
	    		"position": "",
	    		"float": "",
	    		"top": "",
	    		"left": "",
	    		"width": ""
	    	});
	    } else if ( $(this).scrollTop() + heightS + parseInt($(".box").css("padding-top"))*1.25 >= header + heightB + parseInt($(".sideContact").css("margin-bottom"))) {
	    	$(".sideContact").css({
	    		"position": "relative",
	    		"float": "",
	    		"width": "",
	    		"top": heightB - $(".sideContact").height(),
	    		"left": "0"
	    	});
	    } else {
	    	$(".sideContact").css({
	    		"margin-top": ""
	    	});
	    	$(".sideContact").css({
	    		"position": "fixed",
	    		"float": "",
	    		"width": widthSide,
	    		"top": marginTSide + parseInt($(".box").css("padding-top")) ,
	    		"left": width + parseInt($(".box").css("padding-left")),
	    	});
	    }
	});
}