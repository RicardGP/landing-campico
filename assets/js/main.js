/*
	Directive by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

var menu;
$.getScript("assets/js/conf.js", function() {
   menu = menuSemi;
});

(function($) {

	skel.breakpoints({
		wide: '(max-width: 1680px)',
		normal: '(max-width: 1280px)',
		narrow: '(max-width: 980px)',
		narrower: '(max-width: 840px)',
		mobile: '(max-width: 736px)',
		mobilep: '(max-width: 480px)'
	});

	$(function() {

		var	$window = $(window),
			$body = $('body');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {

				// Load the menu into the div.header-promo
				$("div.header-promo").append(menu);

				var locate = window.location.pathname;
				var pag = locate.substring(locate.lastIndexOf("/")+1,locate.search(".html"));


				switch (pag){
					case "informatica":
						$("li#informaticaM").addClass("active")
						break;

					case "jardineria":
						$("li#jardineriaM").addClass("active")
						break;

					case "dam":
						$("li#damM").addClass("active")
						break;

					case "forestal":
						$("li#forestalM").addClass("active")
						break;
				}

				// Before all is loaded
				$body.removeClass('is-loading');
			});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on narrower.
			skel.on('+narrower -narrower', function() {
				$.prioritize(
					'.important\\28 narrower\\29',
					skel.breakpoint('narrower').active
				);
			});

	});

})(jQuery);