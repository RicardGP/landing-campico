$("form").validate({
        errorPlacement: function(error, element) {
            if (element.attr("name") == "lopd[]" )
                error.insertAfter("#formLOPD");
            else
                error.insertAfter(element);
            },
        ignore: ".ignore",
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            tel: {
                required: true,
                minlength: 9
            },
            email: {
                required: true,
                email: true
            },
            subject: {
            	required: true,
            	minlength: 2
            },
            body:{
            	required: true,
            	minlength: 2
            },
            'lopd[]':{
                required: true
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
    });